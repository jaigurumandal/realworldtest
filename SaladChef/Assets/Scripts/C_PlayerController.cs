﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class C_PlayerController : MonoBehaviour
{
    private static C_PlayerController instance;
    public static C_PlayerController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<C_PlayerController>();
            return instance;
        }
    }
    bool pickUp=false;
   public bool rightHandPicked;
   public bool leftHandPicked;
    string RightHandpickedObjectName;
    string LeftHandpickedObjectName;
    public GameObject vegCollector;
    public GameObject choppedVegPrefab;
    public GameObject rightHand;
    public GameObject leftHand;
    public Text g_choppedVeg;
    bool deliveredVeg;
    string g_deliveredVegToCustomer;
    public float g_PlayerScore;
    public Text g_winLossText;
    public Text g_totalScorePopUpText;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if(deliveredVeg)
        {

            if(C_CustomerManager.Instance.g_ranValue==0 && g_deliveredVegToCustomer.Length==3)
            {
                if(g_deliveredVegToCustomer.Contains("A") && g_deliveredVegToCustomer.Contains("C") && g_deliveredVegToCustomer.Contains("F"))
                {
                    Debug.Log("-------------------Won the Match");
                    g_PlayerScore += 80;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Won The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();

                }
                else
                {
                    g_PlayerScore -= 20;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Loss The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }
            else if (C_CustomerManager.Instance.g_ranValue == 1 && g_deliveredVegToCustomer.Length == 3)
            {
                if (g_deliveredVegToCustomer.Contains("A") && g_deliveredVegToCustomer.Contains("D") && g_deliveredVegToCustomer.Contains("F"))
                {
                    g_PlayerScore += 80;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Won The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                else
                {
                    g_PlayerScore -= 20;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Loss The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }
            else if (C_CustomerManager.Instance.g_ranValue == 2 && g_deliveredVegToCustomer.Length == 2)
            {
                if (g_deliveredVegToCustomer.Contains("C") && g_deliveredVegToCustomer.Contains("E") )
                {
                    g_PlayerScore += 80;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Won The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                else
                {
                    g_PlayerScore -= 20;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Loss The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }
            else if (C_CustomerManager.Instance.g_ranValue == 3 && g_deliveredVegToCustomer.Length == 3)
            {
                if (g_deliveredVegToCustomer.Contains("B") && g_deliveredVegToCustomer.Contains("E") && g_deliveredVegToCustomer.Contains("F"))
                {
                    g_PlayerScore += 80;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Won The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                else
                {
                    g_PlayerScore -= 20;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Loss The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }
            else if (C_CustomerManager.Instance.g_ranValue == 4 && g_deliveredVegToCustomer.Length == 3)
            {
                if (g_deliveredVegToCustomer.Contains("A") && g_deliveredVegToCustomer.Contains("C") && g_deliveredVegToCustomer.Contains("D"))
                {
                    g_PlayerScore += 80;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Won The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                else
                {
                    g_PlayerScore -= 20;
                    C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                    C_CustomerManager.Instance.f_WinOrLossPopUp();
                    g_winLossText.text = "You Loss The Game !!";
                    g_totalScorePopUpText.text = g_PlayerScore.ToString();
                }
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }
            else if(g_deliveredVegToCustomer.Length>3)
            {
                g_PlayerScore -= 20;
                C_UImanager.Instance.g_score.text = g_PlayerScore.ToString();
                C_CustomerManager.Instance.f_WinOrLossPopUp();
                g_winLossText.text = "You Loss The Game !!";
                g_totalScorePopUpText.text = g_PlayerScore.ToString();
                g_deliveredVegToCustomer = "";
                deliveredVeg = false;
            }

        }



    }

   

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.CompareTag("Veg") && Input.GetKeyDown(KeyCode.Space))
        {


            if (collider.gameObject.name == "A")
            {
                f_PickUpVeg(collider.gameObject.name);
            }
            else if (collider.gameObject.name == "B")
            {
                f_PickUpVeg(collider.gameObject.name);
            }
            else if (collider.gameObject.name == "C")
            {
                f_PickUpVeg(collider.gameObject.name);
            }
            else if (collider.gameObject.name == "D")
            {
                f_PickUpVeg(collider.gameObject.name);
            }
            else if (collider.gameObject.name == "E")
            {
                f_PickUpVeg(collider.gameObject.name);
            }
            else if (collider.gameObject.name == "F")
            {
                f_PickUpVeg(collider.gameObject.name);
            }





           


        }

        if(collider.gameObject.CompareTag("ChoppingBoard") && Input.GetKeyDown(KeyCode.Space))
        {
            if (rightHand.transform.childCount>0)
            {
                g_choppedVeg.text+= rightHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text;
                Destroy(rightHand.transform.GetChild(0).gameObject);
                rightHandPicked = false;

            }
            else 
            {
                g_choppedVeg.text += leftHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text;
                Destroy(leftHand.transform.GetChild(0).gameObject);
                leftHandPicked = false;
            }




        }



        if (collider.gameObject.CompareTag("Plate") && Input.GetKeyDown(KeyCode.Space))
        {
            if (transform.childCount < 3)
            {
                var choppedVeg = Instantiate(choppedVegPrefab, transform.position, transform.rotation);
                choppedVeg.transform.parent = transform;
                transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Text>().text = g_choppedVeg.text;
                choppedVeg.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            g_deliveredVegToCustomer = transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Text>().text;

        }



        if (collider.gameObject.CompareTag("Customer") && Input.GetKeyDown(KeyCode.Space))
        {

            deliveredVeg = true;
                Destroy(transform.GetChild(2).gameObject);

            g_choppedVeg.text = "";

        }


    }



    public void f_PickUpVeg(string veg)
    {
        if (!string.IsNullOrEmpty(veg))
        {


            if (!rightHandPicked && rightHand.transform.childCount<=1)
            {
                //
                var vegPicked = Instantiate(vegCollector, rightHand.transform.position, rightHand.transform.rotation);
                vegPicked.transform.parent = rightHand.transform;
                vegPicked.transform.localScale = new Vector3(1, 1, 1);
                Debug.Log("here we can picked from right hand----"+veg+"----");
                Debug.Log("Child count in right hand----"+ rightHand.transform.childCount);
                // rightHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = veg;
                vegPicked.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = veg;
                rightHandPicked = true;
            }
            else if (!leftHandPicked && rightHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text != veg && leftHand.transform.childCount<=1)
            {
                //
                Debug.Log("here we can pick up from left hand----" + veg + "----");
                var vegPicked = Instantiate(vegCollector, leftHand.transform.position, leftHand.transform.rotation);
                vegPicked.transform.parent = leftHand.transform;
                vegPicked.transform.localScale = new Vector3(1, 1, 1);
               
               // leftHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = veg;
                vegPicked.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = veg;
                leftHandPicked = true;
            }
            else
            {
                if (rightHandPicked && rightHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text == veg)
                {

                    Destroy(rightHand.transform.GetChild(0).gameObject);
                    rightHandPicked = false;

                }
                else if (leftHandPicked && leftHand.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text == veg)
                {
                    Destroy(leftHand.transform.GetChild(0).gameObject);
                    leftHandPicked = false;
                }
            }
        }
        
          
           
       
    }

   public void f_ResetOnLoss()
    {
        if (transform.childCount>2)
        {
            Destroy(transform.GetChild(2).gameObject);

            g_choppedVeg.text = "";
        }
        
    }
   
}
