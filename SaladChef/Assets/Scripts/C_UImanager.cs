﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class C_UImanager : MonoBehaviour
{

    private static C_UImanager instance;
    public static C_UImanager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<C_UImanager>();
            return instance;
        }
    }

    public Text g_time;
    public Text g_score;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!C_CustomerManager.Instance.popUp)
        {
            g_time.text = (Time.time).ToString();
        }
        
    }
}
