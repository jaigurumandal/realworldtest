﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class C_CustomerManager : MonoBehaviour
{

    private static C_CustomerManager instance;
    public static C_CustomerManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<C_CustomerManager>();
            return instance;
        }
    }

    public List<GameObject> g_Customers;
   public List<Image> g_healthBars;
    private float g_health;
    public float g_startHealth = 100;
     float g_healthDecreaseSpeed = 0.01f;
    public int g_ranValue=0;
    public GameObject g_WinLossPopUp;
    public bool popUp;
    void Start()
    {
        g_health = g_startHealth;
        g_healthBars[0].fillAmount = g_health / g_startHealth;
    }

    // Update is called once per frame
    void Update()
    {

        if (g_health <= 0)
        {

            f_WinOrLossPopUp();
           
           C_PlayerController.Instance.g_winLossText.text = "You Loss The Game !!";
            C_PlayerController.Instance.g_PlayerScore -= 40;
            C_UImanager.Instance.g_score.text = C_PlayerController.Instance.g_PlayerScore.ToString();
            C_PlayerController.Instance.g_totalScorePopUpText.text = C_PlayerController.Instance.g_PlayerScore.ToString();
            C_PlayerController.Instance.f_ResetOnLoss();
            if(C_PlayerController.Instance.rightHand.transform.childCount>0 || C_PlayerController.Instance.leftHand.transform.childCount > 0)
            {
                Destroy(C_PlayerController.Instance.rightHand.transform.GetChild(0).gameObject);
                C_PlayerController.Instance.rightHandPicked = false;
                Destroy(C_PlayerController.Instance.leftHand.transform.GetChild(0).gameObject);
                C_PlayerController.Instance.leftHandPicked = false;
            }
            
            g_health = g_startHealth;
        }
        else if (g_health > 0)
        {
            if (!popUp)
            {
                g_health -= g_healthDecreaseSpeed;
                // Debug.Log(g_health);
                g_healthBars[g_ranValue].fillAmount = g_health / g_startHealth;
            }
            
        }
        

    }

    public void f_CreateRandomCustomer()
    {
        for (int i = 0; i < 5; i++)
        {
            g_Customers[i].SetActive(false);
        }

        g_health = g_startHealth;
        g_ranValue = Random.Range(0, 5);
        g_Customers[g_ranValue].SetActive(true);
        //g_healthBars[g_ranValue].fillAmount = 1;
        Debug.Log("Random Value=" + g_ranValue);
    }


    public void f_WinOrLossPopUp()
    {
        popUp = true;
        g_WinLossPopUp.SetActive(true);
    }


    public void f_PlayAgain()
    {
        popUp = false;
        f_CreateRandomCustomer();
        g_WinLossPopUp.SetActive(false);
    }
}
